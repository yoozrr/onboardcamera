var fs = require('fs')
const path = require('path')
const moment = require('moment')
const promise = require('bluebird')
const skipper = require('skipper-better-s3')
// var onoff = require('onoff')

fs = promise.promisifyAll(fs)

const PMID = process.env.PMID || 'SW202'
const READFOLDER = process.env.READFOLDER || './'
const CHECK_INTERVAL = process.env.CHECK_INTERVAL || 3000
const LOCAL_MEDIA_DIRECTORY = process.env.LOCAL_MEDIA_DIRECTORY || 'devMedia'
const CONCURRENT = parseInt(process.env.CONCURRENT || 10)

const options = { // This is the usual stuff
  adapter: skipper,
  key: 'AKIAJE7YXLTOAH56Q4DA',
  secret: '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',
  region: 'ap-southeast-1',
  bucket: 'static.swiftlogistics.com.my',
  // Let's use the custom s3params to upload this file as publicly
  // readable by anyone,
  s3params: { ACL: 'public-read' },
  // And while we are at it, let's monitor the progress of this upload
  onProgress: progress => console.log('Upload progress:', progress)
}
const adapter = require('skipper-better-s3')(options)
var interval
var uploadMode = process.env.UPLOAD_MODE || 'JPG_ONLY';
// var Gpio = onoff.Gpio
// var led = new Gpio(4, 'out')

/*
 *  Helper Functions
 */
 const blinkLed = function() {
    try {
      var value = (led.readSync() + 1) % 2 //#D
      led.write(value, function() { //#E
        console.log("Changed LED state to: " + value)
      })
    } catch (error) {
      console.log("No LED to send: " + error)
    }
 }

 const walkSync = function(dir, filelist) {
   fs.readdirSync(dir).forEach(function(file) {
     var fileStats = fs.statSync(path.join(dir, file))

// console.log(file, fileStats, path.join(dir, file));
     filelist = fileStats.isDirectory() ?
       walkSync(path.join(dir, file), filelist) :
       file.substr(-4) === '.jpg' ?
         filelist.concat({ mtime: fileStats.birthtime, fname: path.join(dir, file), dir: dir }) :
         filelist
   });

   return filelist
 };


/*
 *  Main Execute function
 */
const execute = function() {
      var files = []
      var localFiles = walkSync(LOCAL_MEDIA_DIRECTORY, [])

      const uploadAndDeleteFile = function(f) {
        var directory = f.dir.replace(LOCAL_MEDIA_DIRECTORY, '')
        var receiver = adapter.receive({ dirname: 'pm/' + PMID + '/' + directory.replace(/^\//, '') })
        const file = fs.createReadStream(f.fname)
// return;
          // Write the file
        return new promise(function(resolve, reject) {
          receiver.write(file, (err) => {
            if (err) {
              console.log('Failed to write...');
              // no need to reject - will try again and delete after complete
              // reject()
              return
            }
            console.log(file.extra.Location)
            fs.unlink(f.fname, function(err) {
              console.log('Deleted file.', f.fname);
              // blinkLed()
              // delete here....
              resolve()
            })
          })
        })
      }

      // Limit 10 only
      localFiles = localFiles.slice(0,CONCURRENT)
      localFiles.map(function(f) {
        if (uploadMode === 'JPG_ONLY') {
          if (f.fname.substr(-4) === '.jpg') {
            files.push(uploadAndDeleteFile(f));
          }
        } else {
          if (f.fname.substr(-4) === '.jpg' || f.fname.substr(-4) === '.mp4') {
            files.push(uploadAndDeleteFile(f));
          }
        }
      })
}

interval = setInterval(execute, 3000)

/*
 *  Clean up on exit.
 */
process.on('exit', function (){
  clearInterval(interval)
  process.exit()
})
