const fs = require('fs')
const path = require('path')
const sqlite3 = require('sqlite3').verbose()
const moment = require('moment')
const promise = require('bluebird')
const _ = require('lodash')
// var onoff = require('onoff')
var db = new sqlite3.Database('sqliteDb')

db = promise.promisifyAll(db)
fs = promise.promisifyAll(fs)

var PMID = process.env.PMID || 'SW202'
var READFOLDER = process.env.READFOLDER || './'
var CHECK_INTERVAL = process.env.CHECK_INTERVAL || 3000

const options = { // This is the usual stuff
  adapter: require('skipper-better-s3'),
  key: 'AKIAJE7YXLTOAH56Q4DA',
  secret: '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',
  region: 'ap-southeast-1',
  bucket: 'static.swiftlogistics.com.my',
  // Let's use the custom s3params to upload this file as publicly
  // readable by anyone,
  s3params: { ACL: 'public-read' },
  // And while we are at it, let's monitor the progress of this upload
  onProgress: progress => console.log('Upload progress:', progress)
}
const adapter = require('skipper-better-s3')(options)
var interval
var dbSerialized = false
// var Gpio = onoff.Gpio
// var led = new Gpio(4, 'out')

/*
 *  Helper Functions
 */
 const blinkLed = function() {
    try {
      var value = (led.readSync() + 1) % 2 //#D
      led.write(value, function() { //#E
        console.log("Changed LED state to: " + value)
      })
    } catch (error) {
      console.log("No LED to send: " + error)
    }
 }

/*
 *  Main Execute function
 */
const execute = function() {
  var currentDate, lastDatetime

  if (!dbSerialized) { return }

  db.getAsync("SELECT updatedAt FROM params WHERE key = 'lastupdated'")
  .then(function(row) {
      console.log(row);

      const receiver = adapter.receive({ dirname: READFOLDER + PMID + '/Camera2/2017-05-29' })
      const file = fs.createReadStream('devMedia/Camera2/2017-05-29/02-16-00.jpg')
      var fileList
      // Check if any files > timestamp
      lastDatetime = new Date(row.updatedAt).getTime();

      const walkSync = function(dir, filelist) {
        fs.readdirSync(dir).forEach(function(file) {
          var fileStats = fs.statSync(path.join(dir, file))

// console.log(file, fileStats, path.join(dir, file));
          filelist = fileStats.isDirectory() ?
            walkSync(path.join(dir, file), filelist) :
            file.substr(-4) === '.jpg' ?
              filelist.concat({ mtime: fileStats.birthtime, fname: path.join(dir, file) }) :
              filelist
        });
        return filelist
      };

      fileList = _(walkSync('./devMedia', []))
        .filter(function(f) {
currentDate = f.mtime
          return f.mtime.getTime() > lastDatetime;
        })
        .sortBy(function(f) {
          return f.mtime;
        }).value()
console.log(fileList);
      return fileList
  })
  .then(function(fileList) {
      console.log(currentDate.toISOString());

      // Promise send

      // fs.readdirAsync(READFOLDER + 'Camera1')
      // .then(function(files) {
      //   console.log(files);
        // Write the file
        // receiver.write(file, () => {
        //   console.log(file.extra)
        //
        //   // Set the db property
        //   // blinkLed()
        // })
      // })
      
      return db.runAsync("UPDATE params SET updatedAt = $updatedAt WHERE key = $key", {
        $updatedAt: moment(lastDatetime).subtract(1, 'month').toDate(),
        $key: 'lastupdated'
      });
  })
  .catch(function(err) {
    // Errored, store the last datetime.
    db.runAsync("UPDATE params SET updatedAt = $updatedAt WHERE key = $key", {
      $updatedAt: moment(lastDatetime).subtract(1, 'month').toDate(),
      $key: 'lastupdated'
    });
  })

}


/*
 *  Start when DB initalized
 */
db.serialize(function() {
  db.run("CREATE TABLE if not exists params (key varchar(25), updatedAt DATETIME)");

  // Double check if 'lastupdated' param is added, otherwise insert.
  db.get("SELECT updatedAt FROM params WHERE key = 'lastupdated'", function(err, row) {
    if (!row) {
      var stmt = db.prepare("INSERT INTO params VALUES (?,?)");
      stmt.run('lastupdated', new Date().toISOString());
      stmt.finalize();
    }
    dbSerialized = true
  })

  // Set the interval
  interval = setInterval(execute, CHECK_INTERVAL)
})



/*
 *  Clean up on exit.
 */
process.on('exit', function (){
  dbSerialized = false
  clearInterval(interval)
  db.close()
  process.exit()
})
